// ==============================================================================================================================
// >>> Easy Database
// ==============================================================================================================================
// easy_database_connect(const String:database_section[], const String:on_connected[], bool:allow_local=true)
// easy_database_query(const String:query[], const String:on_executed[], any:data=0)
// easy_database_fast_query(const String:query[], const String:on_executed[], any:data=0)
// easy_database_query_all(const String:query[], const String:on_executed[], any:data=0)
// easy_database_query_none(const String:query[], const String:on_executed[], any:data=0)
// easy_database_get_handle()

// ==============================================================================================================================
// >>> Source Code
// ==============================================================================================================================

#define MAX_LENGTH_EASY_DATABASE_CALLBACK	128
#define MAX_LENGTH_EASY_DATABASE_QUERY		1024

new Handle:		easy_database_handle;

// ************************************************************************************
stock easy_database_connect(const String:database_section[], const String:on_connected[], bool:allow_local=true)
{
	if ( SQL_CheckConfig(database_section) ) {
		new Handle:datapack = CreateDataPack();
		WritePackString(datapack, on_connected);
		SQL_TConnect(easy_database_on_tconnect, database_section, datapack);
	}
	else if ( allow_local ) {
		decl String:error_message[128];
		easy_database_handle = SQLite_UseDatabase(database_section, error_message, sizeof(error_message));
		easy_database_call_on_connect(on_connected, easy_database_handle, error_message);
	}
	else {
		decl String:error_message[128];
		FormatEx(error_message, sizeof(error_message), "section \"%s\" not found in databases.cfg", database_section);
		easy_database_handle = INVALID_HANDLE;
		easy_database_call_on_connect(on_connected, easy_database_handle, error_message);
	}
}

public easy_database_on_tconnect(Handle:owner, Handle:result, const String:error_message[], any:datapack)
{
	ResetPack(datapack);
	decl String:on_connected[MAX_LENGTH_EASY_DATABASE_CALLBACK];
	ReadPackString(datapack, on_connected, sizeof(on_connected));
	CloseHandle(datapack);
	
	easy_database_handle = result;
	easy_database_call_on_connect(on_connected, result, error_message);
}

easy_database_call_on_connect(const String:on_connected[], Handle:database, const String:error_message[])
{
	if ( on_connected[0] ) {
		new Function:fn = GetFunctionByName(INVALID_HANDLE, on_connected);
		if ( fn != INVALID_FUNCTION ) {
			Call_StartFunction(INVALID_HANDLE, fn);
			Call_PushCell(database);
			Call_PushString(error_message);
			Call_Finish();
		}
		else {
			ThrowError("Callback `%s` not found", on_connected);
		}
	}
}

// ************************************************************************************
stock Handle:easy_database_get_handle()
{
	return easy_database_handle;
}

// ************************************************************************************
stock easy_database_query(const String:query[], const String:on_executed[]="", any:data=0)
{
	dbgMsg("easy_database_query on_executed = %s", on_executed);
	new Handle:datapack = CreateDataPack();
	WritePackString(datapack, on_executed);
	WritePackCell(datapack, data);
	WritePackString(datapack, query);
	SQL_TQuery(easy_database_handle, easy_database_on_query, query, datapack);
	dbgMsg("easy_database_query 4");
}

public easy_database_on_query(Handle:owner, Handle:result, const String:error_message[], any:datapack)
{
	dbgMsg("easy_database_on_query");
	ResetPack(datapack);
	decl String:on_executed[MAX_LENGTH_EASY_DATABASE_CALLBACK];
	ReadPackString(datapack, on_executed, sizeof(on_executed));
	new data = ReadPackCell(datapack);
	
	if ( !result ) {
		decl String:query[MAX_LENGTH_EASY_DATABASE_QUERY];
		ReadPackString(datapack, query, sizeof(query));
		CloseHandle(datapack);
		
		ThrowError("Error during easy_database_query:\n\"%s\"\n\"%s\"", query, error_message);
		return;
	}

	dbgMsg("easy_database_on_query 4");
	CloseHandle(datapack);
	
	if ( SQL_FetchRow(result) ) {
		easy_database_call_on_executed(on_executed, result, data);
	}
	else {
		dbgMsg("easy_database_on_query SQL_FetchRow(NO result)");
		easy_database_call_on_executed(on_executed, INVALID_HANDLE, data);
	}
}

easy_database_call_on_executed(const String:on_executed[], Handle:result, any:data)
{
	dbgMsg("easy_database_call_on_executed");
	if ( on_executed[0] != 0 ) {
		new Function:fn = GetFunctionByName(INVALID_HANDLE, on_executed);
		if ( fn != INVALID_FUNCTION ) {
			Call_StartFunction(INVALID_HANDLE, fn);
			Call_PushCell(result);
			Call_PushCell(data);
			Call_Finish();
		}
		else {
			ThrowError("Callback \"%s\" not found", on_executed);
		}
	}
}

// ************************************************************************************
stock easy_database_fast_query(const String:query[], const String:on_executed[]="", any:data=0)
{
	new Handle:datapack = CreateDataPack();
	WritePackString(datapack, on_executed);
	WritePackCell(datapack, data);
	WritePackString(datapack, query);
	SQL_TQuery(easy_database_handle, easy_database_on_fast_query, query, datapack);
}

public easy_database_on_fast_query(Handle:owner, Handle:result, const String:error_message[], any:datapack)
{
	ResetPack(datapack);
	decl String:on_executed[MAX_LENGTH_EASY_DATABASE_CALLBACK];
	ReadPackString(datapack, on_executed, sizeof(on_executed));
	new data = ReadPackCell(datapack);
	
	if ( !result ) {
		decl String:query[MAX_LENGTH_EASY_DATABASE_QUERY];
		ReadPackString(datapack, query, sizeof(query));
		CloseHandle(datapack);
		
		ThrowError("Error during easy_database_fast_query:\n\"%s\"\n\"%s\"", query, error_message);
	}
	CloseHandle(datapack);
	
	easy_database_call_on_executed(on_executed, INVALID_HANDLE, data);
}

// ************************************************************************************
stock easy_database_query_all(const String:query[], const String:on_executed[]="", any:data=0)
{
	new Handle:datapack = CreateDataPack();
	WritePackString(datapack, on_executed);
	WritePackCell(datapack, data);
	WritePackString(datapack, query);
	SQL_TQuery(easy_database_handle, easy_database_on_query_all, query, datapack);
}

public easy_database_on_query_all(Handle:owner, Handle:result, const String:error_message[], any:datapack)
{
	ResetPack(datapack);
	decl String:on_executed[MAX_LENGTH_EASY_DATABASE_CALLBACK];
	ReadPackString(datapack, on_executed, sizeof(on_executed));
	new data = ReadPackCell(datapack);
	
	if ( !result ) {
		decl String:query[MAX_LENGTH_EASY_DATABASE_QUERY];
		ReadPackString(datapack, query, sizeof(query));
		CloseHandle(datapack);
		
		ThrowError("Error during easy_database_query_all:\n\"%s\"\n\"%s\"", query, error_message);
		return;
	}
	CloseHandle(datapack);
	
	easy_database_call_on_executed(on_executed, result, data);
}

// ************************************************************************************
stock easy_database_query_none(const String:query[], const String:on_executed[]="", any:data=0)
{
	new Handle:datapack = CreateDataPack();
	WritePackString(datapack, on_executed);
	WritePackCell(datapack, data);
	WritePackString(datapack, query);
	SQL_TQuery(easy_database_handle, easy_database_on_query_none, query, datapack);
}

public easy_database_on_query_none(Handle:owner, Handle:result, const String:error_message[], any:datapack)
{
	ResetPack(datapack);
	decl String:on_executed[MAX_LENGTH_EASY_DATABASE_CALLBACK];
	ReadPackString(datapack, on_executed, sizeof(on_executed));
	new data = ReadPackCell(datapack);
	
	if ( !result ) {
		decl String:query[MAX_LENGTH_EASY_DATABASE_QUERY];
		ReadPackString(datapack, query, sizeof(query));
		CloseHandle(datapack);
		
		ThrowError("Error during easy_database_query_none:\n\"%s\"\n\"%s\"", query, error_message);
		return;
	}
	CloseHandle(datapack);
	
	easy_database_call_on_executed(on_executed, INVALID_HANDLE, data);
}

// ************************************************************************************
stock easy_database_queryf(const String:query[], const String:on_executed[]="", any:data=0, any:...)
{
	decl String:formatted_query[MAX_LENGTH_EASY_DATABASE_QUERY];
	VFormat(formatted_query, sizeof(formatted_query), query, 4);
	easy_database_query(formatted_query, on_executed, data);
}

stock easy_database_fast_queryf(const String:query[], const String:on_executed[]="", any:data=0, any:...)
{
	decl String:formatted_query[MAX_LENGTH_EASY_DATABASE_QUERY];
	VFormat(formatted_query, sizeof(formatted_query), query, 4);
	easy_database_fast_query(formatted_query, on_executed, data);
}

stock easy_database_query_allf(const String:query[], const String:on_executed[]="", any:data=0, any:...)
{
	decl String:formatted_query[MAX_LENGTH_EASY_DATABASE_QUERY];
	VFormat(formatted_query, sizeof(formatted_query), query, 4);
	easy_database_query_all(formatted_query, on_executed, data);
}

stock easy_database_query_nonef(const String:query[], const String:on_executed[]="", any:data=0, any:...)
{
	decl String:formatted_query[MAX_LENGTH_EASY_DATABASE_QUERY];
	VFormat(formatted_query, sizeof(formatted_query), query, 4);
	easy_database_query_none(formatted_query, on_executed, data);
}