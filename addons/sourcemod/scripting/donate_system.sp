// цена за день рассчитывается по формуле
// админка 20/корень_из(iDay+20)
// випка 23/корень_из(iDay+20)

// команда sm_addmoney "рубли, стимид, промокод"
// оповещение на сервере игрока в момент зачисления, и оповещение в консоль если игрок на сервере.

// Логирование покупок кто что и когда купил с какой скидкой.

// Покупка iCredits кредитов:
// цена = корень_из(iCredits*0,00001) * 2000

// Промокод. Кто купил (adm1) админку может раздавать промокод, и если кто-то (adm2) будет ложить бабки через промокод, то adm1 получит 3%, а у adm2 будет скидка 5%
// После использования у adm1 генерируется другой промокод чтобы кто-то другой не воспользовался, у adm2 как и у всех новых донатеров генерируется новый промокод.
// В базе данных всегда сохраняется тот кто пригласил по промокоду, и при последующих пополнениях баланса adm1 будет начисляться 3%, но у adm2 уже не будет скидки.

// ==============================================================================================================================
// >>> GLOBAL INCLUDES
// ==============================================================================================================================
#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>


// дебаг сообщение
#define DEBUG
#if defined DEBUG
stock void debugMessage(const char[] message, any ...)
{
	char szMessage[256], szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "logs/donate_system.txt");
	VFormat(szMessage, sizeof(szMessage), message, 2);
	LogToFile(szPath, szMessage);
}
#define dbgMsg(%0) debugMessage(%0)
#else
#define dbgMsg(%0)
#endif

#include <easy_database>

// ==============================================================================================================================
// >>> PLUGIN INFORMATION
// ==============================================================================================================================
#define PLUGIN_VERSION "1.0"
public Plugin:myinfo =
{
	name 			= "Donate System",
	author 			= "AlexTheRegent",
	description 	= "",
	version 		= PLUGIN_VERSION,
	url 			= ""
}

// ==============================================================================================================================
// >>> DEFINES
// ==============================================================================================================================
#pragma newdecls required
#define MPS 		MAXPLAYERS+1
#define PMP 		PLATFORM_MAX_PATH
#define MTF 		MENU_TIME_FOREVER
#define CID(%0) 	GetClientOfUserId(%0)
#define UID(%0) 	GetClientUserId(%0)
#define SZF(%0) 	%0, sizeof(%0)
#define LC(%0) 		for ( int %0 = 1; %0 <= MaxClients; ++%0 ) if ( IsClientInGame(%0) ) 

#define CHAT_NONE		0
#define CHAT_CREDITS	1
#define CHAT_REFERRAL	2

#define PURCHASE_ADMIN	0
#define PURCHASE_VIP	1
#define PURCHASE_TOTAL	2

#define DIVISOR_ADMIN	20
#define DIVISOR_VIP		23

// ==============================================================================================================================
// >>> CONSOLE VARIABLES
// ==============================================================================================================================


// ==============================================================================================================================
// >>> GLOBAL VARIABLES
// ==============================================================================================================================
Menu 		g_mainMenu;
Menu 		g_creditsMenu;

char		g_auth[MPS][32];

int 		g_id[MPS];
int 		g_balance[MPS];
int			g_chatInput[MPS];
int 		g_purchaseDate[MPS][PURCHASE_TOTAL];
int			g_privilegeDuration[MPS][PURCHASE_TOTAL];
int			g_referrals[MPS];

// ==============================================================================================================================
// >>> LOCAL INCLUDES
// ==============================================================================================================================


// ==============================================================================================================================
// >>> FORWARDS
// ==============================================================================================================================
public void OnPluginStart() 
{
	easy_database_connect("donate_system", "on_database_connect");
	
	RegAdminCmd("sm_addmoney", Command_AddMoney, ADMFLAG_ROOT);
	RegConsoleCmd("sm_donate", Command_DonateSystem);
	
	g_mainMenu = new Menu(Handler_MainMenu);
	g_mainMenu.SetTitle("Donate System\n \n");
	g_mainMenu.AddItem("credits", 	"Купить кредиты");
	g_mainMenu.AddItem("admin", 	"Купить админку");
	g_mainMenu.AddItem("vip", 		"Купить вип");
	// g_mainMenu.AddItem("referral", 	"Получить одноразовую скидку 5%");
	
	char buffer[128];
	g_creditsMenu = new Menu(Handler_CreditsMenu);
	g_creditsMenu.SetTitle("Покупка кредитов\n \n");
	FormatEx(SZF(buffer), "1000 кредитов (%d)", GetCreditsPrice(1000));
	g_creditsMenu.AddItem("1000", buffer);
	FormatEx(SZF(buffer), "2000 кредитов (%d)", GetCreditsPrice(2000));
	g_creditsMenu.AddItem("2000", buffer);
	FormatEx(SZF(buffer), "5000 кредитов (%d)", GetCreditsPrice(5000));
	g_creditsMenu.AddItem("5000", buffer);
	g_creditsMenu.AddItem("custom", "Подсчитать цену для своего числа");
}

public void OnMapStart() 
{
	
}

public void OnConfigsExecuted() 
{
	
}

public void OnClientPutInServer(int client)
{
	g_id[client] = 0;
	g_balance[client] = 0;
	g_chatInput[client] = CHAT_NONE;
	
	GetClientAuthId(client, AuthId_Steam2, g_auth[client], sizeof(g_auth[]));
	easy_database_queryf("SELECT `id`, `balance` FROM `d_clients` WHERE `auth` = '%s' LIMIT 1;", "OnGotClientData", UID(client), g_auth[client]);
}

public void OnGotClientData(DBResultSet result, any userid)
{
	int client = CID(userid);
	if ( client == 0 ) return;
	
	if ( result ) {
		g_id[client] = result.FetchInt(0);
		g_balance[client] = result.FetchInt(1);
		
		easy_database_queryf("SELECT `date`, `duration` FROM `d_admin_purchases` WHERE `id` = %d LIMIT 1;", "OnGotClientAdminPurchases", UID(client), g_id[client]);
		easy_database_queryf("SELECT `date`, `duration` FROM `d_vip_purchases` WHERE `id` = %d LIMIT 1;", "OnGotClientVipPurchases", UID(client), g_id[client]);
		easy_database_queryf("SELECT `referrals` FROM `d_refcodes` WHERE `id` = %d LIMIT 1;", "OnGotClientVipPurchases", UID(client), g_id[client]);
	}
}

public void OnGotClientVipPurchases(DBResultSet result, any userid)
{
	int client = CID(userid);
	if ( client == 0 ) return;
	
	g_purchaseDate[client][PURCHASE_ADMIN] = result?result.FetchInt(0):0;
	g_privilegeDuration[client][PURCHASE_ADMIN] = result?result.FetchInt(1):0;
}

public void OnGotClientAdminPurchases(DBResultSet result, any userid)
{
	int client = CID(userid);
	if ( client == 0 ) return;
	
	g_purchaseDate[client][PURCHASE_VIP] = result?result.FetchInt(0):0;
	g_privilegeDuration[client][PURCHASE_VIP] = result?result.FetchInt(1):0;
}

public void OnGotClientReferrals(DBResultSet result, any userid)
{
	int client = CID(userid);
	if ( client == 0 ) return;
	
	g_referrals[client] = result?result.FetchInt(0):0;
}

// ==============================================================================================================================
// >>> 
// ==============================================================================================================================
public void on_database_connect(Handle database, const char[] error)
{
	easy_database_query_none("CREATE TABLE IF NOT EXISTS `d_clients` (`id` INTEGER AUTO_INCREMENT PRIMARY KEY, `auth` VARCHAR(32), `balance` INTEGER);", "on_table_created");
	easy_database_query_none("CREATE TABLE IF NOT EXISTS `d_refcodes` (`code` VARCHAR(32) PRIMARY KEY, `referral_auth` VARCHAR(32) , `referrals` INT);");
	easy_database_query_none("CREATE TABLE IF NOT EXISTS `d_admin_purchases` (`id` INTEGER , `date` INTEGER, `duration` INTEGER);");
	easy_database_query_none("CREATE TABLE IF NOT EXISTS `d_vip_purchases` (`id` INTEGER , `date` INTEGER, `duration` INTEGER);");
	easy_database_query_none("CREATE TABLE IF NOT EXISTS `d_referrals` (`id` INTEGER , `referral_auth` VARCHAR(32), `code` VARCHAR(32));");
	easy_database_query_none("CREATE TABLE IF NOT EXISTS `d_logs` (`id` INTEGER, `message` VARCHAR(512));");
}

public void on_table_created(Handle result, any data)
{
	LC(i) {
		OnClientPutInServer(i);
	}
}

// ==============================================================================================================================
// >>> 
// ==============================================================================================================================
public Action Command_AddMoney(int client, int argc)
{
	char auth[32], credits[32], refcode[32];
	GetCmdArg(1, SZF(auth));
	GetCmdArg(2, SZF(credits));
	GetCmdArg(3, SZF(refcode));
	if (StrEqual(":", refcode)) {
		PrintToConsole(client, "стимид должен быть в кавычках");
		return Plugin_Handled;
	}
	AddMoney(client, auth, StringToInt(credits), refcode, true);
	return Plugin_Handled;
}
	
void AddMoney(const int caller, const char[] auth, const int amount, const char[] refcode, bool lookForRef) {
	Handle datapack = CreateDataPack();
	WritePackCell(datapack, caller? UID(caller):0);
	WritePackCell(datapack, amount);
	WritePackString(datapack, auth);
	WritePackString(datapack, refcode);
	WritePackCell(datapack, lookForRef);
	
	easy_database_queryf("SELECT c.id, c.balance, r.referral_auth FROM d_clients AS c INNER JOIN d_referrals AS r ON r.id = c.id WHERE `auth` = '%s' LIMIT 1 UNION SELECT `referral_auth`, 0, 0 FROM `d_refcodes` WHERE `code` = '%s' LIMIT 1;", "OnSetClientBalance", datapack, auth, refcode);
}

public void OnSetClientBalance(DBResultSet result, any datapack) 
{
	ResetPack(datapack);
	int caller = CID(ReadPackCell(datapack));
	
	int amount = ReadPackCell(datapack);
	
	char auth[32];
	ReadPackString(datapack, SZF(auth));
	dbgMsg("auth-%s, amount-%i", auth, amount);
	char refcode[32];
	ReadPackString(datapack, SZF(refcode));
	
	char referral_auth[32];
	bool lookForRef = ReadPackCell(datapack);
	if ( lookForRef && result && result.FieldCount == 3 ) {
		result.FetchString(2, SZF(referral_auth));
		AddMoney(0, referral_auth, RoundToNearest(amount*0.03), "", false);
	}
	
	if ( refcode[0] == 0 ) {
		if ( result ) {
			int id = result.FetchInt(0);
			int old_balance = result.FetchInt(1);
			
			easy_database_query_nonef("UPDATE `d_clients` SET `balance` = %d WHERE `id` = %d;", "OnUpdateClientBalance", datapack, old_balance+amount, id);
		}
		else {
			char code[32];
			IntToString(GetTime(), SZF(code));
			GenerateCode(code);
			
			easy_database_query_nonef("INSERT INTO `d_clients`(`auth`, `balance`) VALUES ('%s', %d);", "OnUpdateClientBalance", datapack, auth, amount);
			dbgMsg("INSERT INTO `d_clients`(`auth`, `balance`) VALUES ('%s', %d);", auth, amount);
		}
	}
	else {
		if ( result ) {
			if ( result.FieldCount > 2 ) {
				if ( caller ) PrintToChat(caller, "Уже является рефералом");
				else PrintToServer("Уже является рефералом");
				return;
			}
			else {
				int id = result.FetchInt(0);
				int old_balance = result.FetchInt(1);
				
				char code[32];
				IntToString(GetTime(), SZF(code));
				GenerateCode(code);
			
				easy_database_query_nonef("UPDATE `d_clients` SET `balance` = %d WHERE `id` = %d;", "OnUpdateClientBalance", datapack, old_balance+(amount*1.05), id);
				easy_database_query_nonef("INSERT INTO `referrals` (`id`, `referral_auth`, `code`) VALUES (%d, '%s', %s);", "", id, referral_auth, refcode);
				easy_database_query_nonef("UPDATE `d_refcodes` SET `code` = '%s', `referrals` = `referrals` + 1 WHERE `referral_auth` = '%s';", "", 0, code, referral_auth);

				dbgMsg("UPDATE `d_clients` SET `balance` = %d WHERE `id` = %d;", old_balance+(amount*1.05), id);
				dbgMsg("INSERT INTO `referrals` (`id`, `referral_auth`, `code`) VALUES (%d, '%s', %s);", referral_auth, refcode);
				dbgMsg("UPDATE `d_refcodes` SET `code` = '%s', `referrals` = `referrals` + 1 WHERE `referral_auth` = '%s';", code, referral_auth);
			}
		}
		else {
			if ( caller ) PrintToChat(caller, "Неверный реферальный код");
			else PrintToServer("Неверный реферальный код");
			return;
		}
	}
}

public void OnUpdateClientReferral(DBResultSet result, any datapack) 
{
	ResetPack(datapack);
	ReadPackCell(datapack);
	ReadPackCell(datapack);
	
	char auth[32];
	ReadPackString(datapack, SZF(auth));
	
	char refcode[32];
	ReadPackString(datapack, SZF(refcode));
	
	easy_database_query_nonef("INSERT INTO `refcodes` (`code`, `referral_auth`, `referrals`) VALUES ('%s', LAST_INSERT_ID(), 0);", "", 0, refcode);
	OnUpdateClientBalance(view_as<DBResultSet>(INVALID_HANDLE), datapack);
}

void GenerateCode(char[] code)
{
	int len = strlen(code);
	for ( int i = 0; i < len; ++i ) {
		switch (code[i]) {
			case '0': code[i] = 'f';
			case '1': code[i] = '_';
			case '2': code[i] = 'l';
			case '3': code[i] = 'k';
			case '4': code[i] = 'D';
			case '5': code[i] = 'A';
			case '6': code[i] = '*';
			case '7': code[i] = '2';
			case '8': code[i] = '+';
			case '9': code[i] = 'w';
			default: code[i] = '?';
		}
	}
}

public void OnUpdateClientBalance(DBResultSet result, any datapack) 
{
	ResetPack(datapack);
	int caller = CID(ReadPackCell(datapack));
	int amount = ReadPackCell(datapack);
	
	char auth[32];
	ReadPackString(datapack, SZF(auth));
	CloseHandle(datapack);
	int target = GetClientByAuth(auth);
	if ( target ) {
		PrintToChat(target, "Ваш баланс пополнен на %d кредитов", amount);
		GetClientName(target, SZF(auth));
	}
	
	if ( caller ) {
		PrintToChat(caller, "Баланс игрока (%s) успешно пополнен", auth);
	}
	else {
		PrintToServer("Баланс игрока (%s) успешно пополнен", auth);
	}
}

// ==============================================================================================================================
// >>> 
// ==============================================================================================================================
void SaveClientBalance(int client)
{
	easy_database_query_nonef("UPDATE `d_clients` SET `balance` = %d WHERE `id` = %d;", "", 0, g_balance[client], g_id[client]);
}

// ==============================================================================================================================
// >>> 
// ==============================================================================================================================
public Action Command_DonateSystem(int client, int argc) 
{
	g_mainMenu.Display(client, MTF);
	return Plugin_Handled;
}

// ==============================================================================================================================
// >>> 
// ==============================================================================================================================
int GetClientByAuth(const char[] target_auth)
{
	char auth[32];
	LC(i) {
		GetClientAuthId(i, AuthId_Steam2, SZF(auth));
		if ( StrEqual(auth[6], target_auth[6]) ) {
			return i;
		}
	}
	return 0;
}

int GetCreditsPrice(int credits)
{
	return RoundToNearest(SquareRoot(credits*0.00001) * 2000);
}

void BuyCredits(int client, int credits)
{
	int price = GetCreditsPrice(credits);
	if ( g_balance[client] >= price ) {
		g_balance[client] -= price;
		SaveClientBalance(client);
		
		ServerCommand("sm_addcredits #%d %d", UID(client), credits);
		WriteLog("Игрок %N(%s|%d) купил %d кредитов за %d кредитов (осталось %d кредитов)", client, g_auth[client], g_id[client], credits, price, g_balance[client]);
	}
	else {
		PrintToChat(client, "Недостаточно средств. Вам нужно ещё %d кредитов", price-g_balance[client]);
	}
}

// ==============================================================================================================================
// >>> 
// ==============================================================================================================================
public int Handler_MainMenu(Menu menu, MenuAction action, int client, int slot)
{
	switch ( action ) {
		case MenuAction_Select: {
			char item[64];
			menu.GetItem(slot, SZF(item));
			
			if ( StrEqual(item, "credits") ) {
				g_creditsMenu.Display(client, MTF);
			}
			else if ( StrEqual(item, "admin") ) {
				DisplayAdminMenu(client);
			}
			else if ( StrEqual(item, "vip") ) {
				DisplayVipMenu(client);
			}
			else if ( StrEqual(item, "referral") ) {
				g_chatInput[client] = CHAT_REFERRAL;
				PrintToChat(client, "Введите код. 0 для отмены.");
			}
		}
	}
}

public int Handler_CreditsMenu(Menu menu, MenuAction action, int client, int slot)
{
	switch ( action ) {
		case MenuAction_Select: {
			char item[64];
			menu.GetItem(slot, SZF(item));
			
			int value = StringToInt(item);
			if ( value == 0 ) {
				g_chatInput[client] = CHAT_CREDITS;
				PrintToChat(client, "Введите число в чат для подсчёта цены. 0 для отмены.");
			}
			else {
				BuyCredits(client, value);
			}
		}
	}
}

// ==============================================================================================================================
// >>> 
// ==============================================================================================================================
void DisplayAdminMenu(int client)
{
	Menu menu = new Menu(Handler_AdminMenu);
	menu.SetTitle("Покупка админки\n \n");
	
	char buffer[128];
	FormatEx(SZF(buffer), "7 дней (%d)", GetPrivilegePrice(client, 7, PURCHASE_ADMIN, DIVISOR_ADMIN));
	menu.AddItem("7", buffer);
	FormatEx(SZF(buffer), "14 дней (%d)", GetPrivilegePrice(client, 14, PURCHASE_ADMIN, DIVISOR_ADMIN));
	menu.AddItem("14", buffer);
	FormatEx(SZF(buffer), "30 дней (%d)", GetPrivilegePrice(client, 30, PURCHASE_ADMIN, DIVISOR_ADMIN));
	menu.AddItem("30", buffer);
	
	menu.Display(client, MTF);
}

void DisplayVipMenu(int client)
{
	Menu menu = new Menu(Handler_VipMenu);
	menu.SetTitle("Покупка випки\n \n");
	
	char buffer[128];
	FormatEx(SZF(buffer), "7 дней (%d)", GetPrivilegePrice(client, 7, PURCHASE_VIP, DIVISOR_VIP));
	menu.AddItem("7", buffer);
	FormatEx(SZF(buffer), "14 дней (%d)", GetPrivilegePrice(client, 14, PURCHASE_VIP, DIVISOR_VIP));
	menu.AddItem("14", buffer);
	FormatEx(SZF(buffer), "30 дней (%d)", GetPrivilegePrice(client, 30, PURCHASE_VIP, DIVISOR_VIP));
	menu.AddItem("30", buffer);
	
	menu.Display(client, MTF);
}

int GetPrivilegePrice(int client, int days, int slot, int divisor)
{
	int days_rem, time_rem;
	time_rem = GetTime() - g_purchaseDate[client][slot] - g_privilegeDuration[client][slot]*86400;
	if ( time_rem > 0 ) {
		days_rem = g_privilegeDuration[client][slot];
	}
	else {
		days_rem = g_privilegeDuration[client][slot] + time_rem/86400;
		if ( days_rem < 0 ) days_rem = 0;
	}
	
	int price = RoundToNearest(divisor/SquareRoot(days+days_rem+20.0));
	return price;
}

public int Handler_AdminMenu(Menu menu, MenuAction action, int client, int slot)
{
	switch ( action ) {
		case MenuAction_Select: {
			char info[16];
			menu.GetItem(slot, SZF(info));
			int days = StringToInt(info);
			
			int price = GetPrivilegePrice(client, days, PURCHASE_ADMIN, DIVISOR_ADMIN);
			if ( g_balance[client] >= price ) {
				g_balance[client] -= price;
				SaveClientBalance(client);
				
				ServerCommand("sm_addvip \"steam\" \"%s\" \"30\" \"VipAdmin\"", g_auth[client], days);
			}
		}
		
		case MenuAction_End: {
			delete menu;
		}
	}
}

public int Handler_VipMenu(Menu menu, MenuAction action, int client, int slot)
{
	switch ( action ) {
		case MenuAction_Select: {
			char info[16];
			menu.GetItem(slot, SZF(info));
			int days = StringToInt(info);
			
			int price = GetPrivilegePrice(client, StringToInt(info), PURCHASE_VIP, DIVISOR_VIP);
			if ( g_balance[client] >= price ) {
				g_balance[client] -= price;
				SaveClientBalance(client);
				
				ServerCommand("sm_addvip \"steam\" \"%s\" \"%d\" \"Admin\"", g_auth[client], days);
			}
		}
		
		case MenuAction_End: {
			delete menu;
		}
	}
}

// ==============================================================================================================================
// >>> 
// ==============================================================================================================================
public void OnClientSayCommand_Post(int client, const char[] command, const char[] args)
{
	switch ( g_chatInput[client] ) {
		case CHAT_CREDITS: {
			int value = StringToInt(args);
			if ( value > 0 ) {
				Menu menu = new Menu(Handler_CreditsMenu);
				menu.SetTitle("Покупка кредитов\n \n");
				
				char item[64], text[128];
				IntToString(value, SZF(item));
				FormatEx(SZF(text), "Купить %d кредитов за %d кредитов", value, GetCreditsPrice(value));
				menu.AddItem(item, text);
				
				menu.Display(client, MTF);
			}
		}
		
		case CHAT_REFERRAL: {
			
		}
	}
}

// ==============================================================================================================================
// >>> 
// ==============================================================================================================================
void WriteLog(char[] message, any ...) 
{
	char log[512];
	VFormat(SZF(log), message, 2);
	easy_database_query_nonef("INSERT INTO `d_logs` (`message`) VALUES ('%s');", log);
}	
